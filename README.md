Les Aventuriers du Rail
====

Description : 
----
Projet réalisé au Corbier, en milieu "hostile" dans le cadre du projet tutoré du semestre n°4.

Equipe projet :
----
 - Justin Allard
 - Alexis Godin
 - Erik Kubiak
 - Robin Lepetit
 - Quentin Marcotte
 - Timothé Pardieu