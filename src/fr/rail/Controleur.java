package fr.rail;

import fr.rail.inter.IDessinable;
import fr.rail.metier.*;
import fr.rail.metier.carte.CarteDestination;
import fr.rail.metier.carte.CarteWagon;
import fr.rail.util.LectureFichier;
import fr.rail.vue.Fenetre;
import fr.rail.vue.FenetreChoix;
import fr.rail.vue.FenetreChoixCarteDest;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Classe principale permettant de faire la passerelle entre la partie métier et la partie fr.rail.vue.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 14/03/2017
 */
public class Controleur {
    private ArrayList<Voie> ensVoie;
    private ArrayList<Ville> ensVille;
    private List<Joueur> ensJoueur;

    private Plateau plateau;

    private Fenetre fenetre;

    private int joueurCourant;

    private int nbCarteTire;

    /**
     * Constructeur du Controleur.
     */
    public Controleur(String[] joueurs) {
        ensVille = LectureFichier.lireVilles();
        ensVoie = LectureFichier.lireVoies(this);
        ensJoueur = new ArrayList<>();

        ECouleur[] couleurs = {ECouleur.VERT, ECouleur.ROUGE, ECouleur.NOIR, ECouleur.JAUNE, ECouleur.BLEU};

        for (int i = 0; i < joueurs.length; i++) {
            int nbJoueursMemeNom = 0;

            for (int j = 0; j < i; j++)
                if (joueurs[i].equals(joueurs[j]))
                    nbJoueursMemeNom++;

            String nomJoueur = joueurs[i];

            if (nbJoueursMemeNom > 0)
                nomJoueur += " (" + nbJoueursMemeNom + ")";

            ensJoueur.add(new Joueur(nomJoueur, couleurs[i]));
        }

        joueurCourant = 0;

        plateau = new Plateau(this);

        fenetre = new Fenetre(this);
    }

    /**
     * @return le nombre de voies
     */
    public int getNbVoie() {
        return ensVoie.size();
    }

    /**
     * @param indice indice de la Voie demandée
     * @return la Voie correspondante à l'indice
     */
    public Voie getVoie(int indice) {
        try {
            return ensVoie.get(indice);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * @return le nombre de villes
     */
    public int getNbVille() {
        return ensVille.size();
    }

    /**
     * @param indice idnice de la Ville demandée
     * @return la Ville correspondante à l'indice
     */
    public IDessinable getVille(int indice) {
        return ensVille.get(indice);
    }

    /**
     * @return l'ensemble de Ville
     */
    public ArrayList<Ville> getEnsVille() {
        return ensVille;
    }

    public boolean peutPrendreVoie(int indiceVoie, boolean estVoieDouble)
    {
        Voie   v = getVoie(indiceVoie);
        Joueur j = ensJoueur.get(joueurCourant);

        if(!j.peutPrendre(v, estVoieDouble))
            return false;

        if ((ensJoueur.size() == 2 || ensJoueur.size() == 3)           &&
                (v.getJoueurVoie1() != null || v.getJoueurVoie2() != null)    )
            return false;

        if (v.getJoueurVoie1() != null &&!estVoieDouble)
            return false;

        if (estVoieDouble && v.getJoueurVoie1() != null && v.getJoueurVoie2() != null)
            return false;

        return true;
    }

    public void prendreVoie(int indiceVoie, boolean estVoieDouble, ArrayList<CarteWagon> ensCarte)
    {
        Voie   v = getVoie(indiceVoie);
        Joueur j = ensJoueur.get(joueurCourant);

        System.out.println(ensCarte);
        for (CarteWagon carteWagon : ensCarte)
        {
            j.ajouterDeltaCarteWagon(carteWagon, -1);
            plateau.jeterCarteWagon(carteWagon);
        }

        if (!estVoieDouble)
            v.setJoueurVoie1(j);
        else
            v.setJoueurVoie2(j);

        j.ajouterDeltaNbWagon(-v.getNbTroncons());
        j.ajouterDeltaScore(calculerScore(v.getNbTroncons()));

        changerJoueurCourant();
        fenetre.recherager();
    }

    private int calculerScore(int nbTroncons) {
        switch (nbTroncons) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 4;
            case 4:
                return 7;
            case 6:
                return 15;
            case 8:
                return 21;
        }

        return Integer.MIN_VALUE;
    }

    /**
     * Change le joueur courant.
     */
    public void changerJoueurCourant() {
        joueurCourant = (joueurCourant + 1) % ensJoueur.size();
        fenetre.changerJoueurCourant();
    }

    /**
     * @return le joueur courant
     */
    public Joueur getJoueurCourant() {
        return ensJoueur.get(joueurCourant);
    }


    public void enregistrerVoie() {
        String ret = "";

        for (int i = 0; i < ensVoie.size(); i++) {
            try {
                ret += ensVoie.get(i).toCsvLine() + "\n";
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        File fichierSortie = new File("donnees/DonneesVoies.csv");
        if (fichierSortie.exists())
            fichierSortie.delete();

        fichierSortie = new File("donnees/DonneesVoies.csv");
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(fichierSortie.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(ret);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        if (args.length < 2 || args.length > 5) {
            System.err.println("Vous devez etre entre 2 et 5 joueurs !");
            System.exit(1);
        }

        new Controleur(args);
    }

    public List<Joueur> getEnsJoueur() {
        return ensJoueur;
    }

    public boolean cheminExistant(Ville ville1, Ville ville2, Joueur joueur) {
        return GPS.cheminExistant(ville1, ville2, joueur, ensVille.size());
    }

    public Joueur getJoueurParNom(String nom) {
        for (Joueur j : ensJoueur) {
            if (j.getPseudo().equalsIgnoreCase(nom))
                return j;
        }
        return null;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void piocherCarteWagon() {
        plateau.piocherCarteWagon(getJoueurCourant());
        nbCarteTire++;

        if (nbCarteTire >= 2) {
            nbCarteTire = 0;
            changerJoueurCourant();
        }

        fenetre.recherager();
    }

    public void piocherCarteDestination() {
        fenetre.setEnabled(false);
        new FenetreChoixCarteDest(this, plateau.piocherCartesDestination(3));
    }

    public void piocherCarteWagon(int indice) {
        if (plateau.getCarteDecouverte().get(indice).getImage().equals("carte_loco") && nbCarteTire > 1)
            return;

        CarteWagon tmp = plateau.piocherCarteWagonDecouverte(getJoueurCourant(), indice);
        nbCarteTire++;

        if (tmp.getImage().equals("carte_loco") || nbCarteTire >= 2) {
            nbCarteTire = 0;
            changerJoueurCourant();
        }

        fenetre.recherager();
    }

    public void garderCartesDest(List<CarteDestination> select, List<CarteDestination> nonSelect) {
        for (CarteDestination carte : nonSelect)
            plateau.mettreCarteDestinationDessous(carte);

        for (CarteDestination carte : select)
            getJoueurCourant().ajouterCarte(carte);

        fenetre.setEnabled(true);

        changerJoueurCourant();
    }

    public void voieAction(int indiceVoie, boolean estVoieDouble) {
        if (peutPrendreVoie(indiceVoie, estVoieDouble)) {
            new FenetreChoix(this, indiceVoie, estVoieDouble);
        }
        else {
            JOptionPane.showMessageDialog(this.fenetre, "Vous ne pouvez pas prendre cette voie !",
                    "Prise Impossible", JOptionPane.ERROR_MESSAGE);
        }
    }

    public boolean carteValides(int numVoie, boolean estVoieDouble, ArrayList<CarteWagon> cartes)
    {
        Voie   v = getVoie(numVoie);
        Joueur j = ensJoueur.get(joueurCourant);

        if (cartes.size() < v.getNbTroncons())
            return false;

        Collections.sort(cartes, new Comparator<CarteWagon>() {
            @Override
            public int compare(CarteWagon o1, CarteWagon o2) {
                return o1.getImage().compareTo(o2.getImage());
            }
        });

        CarteWagon carteWagon = cartes.get(0);
        int nbCarteSimple = 1;
        int nbCarteLoco = 0;

        for (int i = 1; i < cartes.size(); i++)
            if (carteWagon.equals(cartes.get(i)))
                nbCarteSimple++;

        for (int i = 0; i < cartes.size(); i++)
            if (CarteWagon.typeCartes[CarteWagon.typeCartes.length - 1].equals(cartes.get(i)))
                nbCarteLoco++;

        if (nbCarteLoco > ensJoueur.get(joueurCourant).getNbCarte(CarteWagon.typeCartes[CarteWagon.typeCartes.length - 1]))
            return false;

        if (nbCarteSimple > ensJoueur.get(joueurCourant).getNbCarte(carteWagon))
            return false;

        if(nbCarteLoco < v.getNbFerries())
            return false;

        if (nbCarteLoco + nbCarteSimple < v.getNbTroncons())
            return false;


        return true;
    }
}
