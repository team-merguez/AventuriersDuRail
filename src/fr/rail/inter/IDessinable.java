package fr.rail.inter;

/**
 * Interface permettant de représenter les élements dessinables.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 13/03/2017
 */
public interface IDessinable
{
    /**
     * @return l'abscisse
     */
    int getX();

    /**
     * @return l'ordonnée
     */
    int getY();
}
