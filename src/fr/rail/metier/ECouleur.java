package fr.rail.metier;

import java.awt.*;

/**
 * Created by alexi on 17/03/2017.
 */
public enum ECouleur {
    BLEU(Color.BLUE, "voiture_bleue.jpg"),
    JAUNE(Color.YELLOW, "voiture_jaune.jpg"),
    NOIR(Color.BLACK, "voiture_noire.jpg"),
    ROUGE(Color.RED, "voiture_rouge.jpg"),
    VERT(Color.GREEN, "voiture_verte.jpg");

    private Color couleur;

    private String voiture;

    ECouleur(Color couleur, String voiture) {
        this.couleur = couleur;
        this.voiture = voiture;
    }

    public Color getCouleur() {
        return couleur;
    }

    public String getVoiture() {
        return voiture;
    }
}
