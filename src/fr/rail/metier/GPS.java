package fr.rail.metier;

import java.util.ArrayList;


/**
 * Cette classe permet de vérifier si deux villes sont reliés par les voies possédées par un joueur donné.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 15/03/2017
 */
public class GPS
{
    public static boolean cheminExistant(Ville ville1, Ville ville2, Joueur j, int nbVille)
    {
        ArrayList<Voie> tmp = chemin(ville1, ville2, j, nbVille);

        return tmp != null && tmp.size() != 0;
    }

    public static ArrayList<Voie> chemin(Ville ville1, Ville ville2, Joueur j, int nbVille)
    {
        return chemin(ville1, ville2, j, nbVille, new ArrayList<Voie>(),new ArrayList<Ville>());
    }

    private static ArrayList<Voie> chemin(Ville sommetCourant   ,  Ville destination, Joueur j, int nbVille,
                                         ArrayList<Voie> chemin, ArrayList<Ville> sommetMarques        )
    {
        if (sommetCourant.equals(destination))
            return chemin;
        if(sommetMarques.size() == nbVille)
            return new ArrayList<Voie>();

        sommetMarques.add(sommetCourant);

        int min = Integer.MAX_VALUE;
        Ville villeTmp;
        ArrayList<Voie> nouveauChemin;
        ArrayList<Voie> tmp;
        ArrayList<Voie> ret = null;

        for (Voie v : sommetCourant.getEnsVoie())
        {
            villeTmp = v.getVille1() != sommetCourant? v.getVille1():v.getVille2();

            if(!sommetMarques.contains(villeTmp) && (v.getJoueurVoie1() == j || v.getJoueurVoie2() == j))
            {
                nouveauChemin = new ArrayList<Voie>(chemin);
                nouveauChemin.add(v);
                tmp = chemin(villeTmp, destination, j, nbVille, nouveauChemin, sommetMarques);
                if (tmp != null && tmp.size() < min)
                {
                    ret = tmp;
                    min = tmp.size();
                }

            }
        }

        return ret;
    }
}
