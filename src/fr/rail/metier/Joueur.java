package fr.rail.metier;

import fr.rail.metier.carte.Carte;
import fr.rail.metier.carte.CarteDestination;
import fr.rail.metier.carte.CarteWagon;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cette classe permet de representer un joueur.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 15/03/2017
 */
public class Joueur {

    /**
     * Le pseudo du joueur.
     */
    private String pseudo;

    private ECouleur couleur;

    private int nbWagon = 45;
    private int score = 0;

    private Map<CarteWagon, Integer> carteWagonsPossedees;

    private List<CarteDestination> carteDestinationsPossedees;

    /**
     * Contructeur d'un joueur.
     *
     * @param pseudo le pseudo du joueur
     */
    public Joueur(String pseudo, ECouleur couleur) {
        this.pseudo = pseudo;
        this.couleur = couleur;

        carteWagonsPossedees = initCarteWagonPossedee();
        carteDestinationsPossedees = new ArrayList<>();
    }

    private Map<CarteWagon, Integer> initCarteWagonPossedee()
    {
        HashMap<CarteWagon, Integer> ret = new HashMap<CarteWagon, Integer>();

        for (CarteWagon carteWagon : CarteWagon.typeCartes)
            ret.put(carteWagon, 0);

        return ret;
    }

    public boolean ajouterCarte(Carte carte) {
        if (carte instanceof CarteWagon)
            return ajouterCarteWagon((CarteWagon) carte);
        else if (carte instanceof CarteDestination)
            return carteDestinationsPossedees.add((CarteDestination) carte);
        else
            return false;
    }

    private boolean ajouterCarteWagon(CarteWagon carte)
    {
        return ajouterDeltaCarteWagon(carte, 1);
    }

    /**
     * @return le pseudo du joueur
     */
    public String getPseudo() {
        return pseudo;
    }

    /**
     * Definit le pseudo du joueur
     *
     * @param pseudo le nouveau pseudo du joueur
     */
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public ECouleur getCouleur() {
        return couleur;
    }

    public String toString() {
        return getPseudo();
    }

    public boolean defausserCarte(CarteDestination carteDestination)
    {
        if (carteDestination.estRouteLongue())
            return false;

        return carteDestinationsPossedees.remove(carteDestination);
    }

    public void ajouterDeltaScore(int delta)
    {
        score += delta;
    }

    public boolean ajouterDeltaNbWagon(int delta)
    {
        if (delta + nbWagon < 0)
            return false;

        nbWagon += delta;
        return true;
    }

    public int getNbWagon() {
        return nbWagon;
    }

    public int getScore() {
        return score;
    }

    public Map<CarteWagon, Integer> getCarteWagonsPossedees() {
        return carteWagonsPossedees;
    }

    public List<CarteDestination> getCarteDestinationsPossedees() {
        return carteDestinationsPossedees;
    }

    public int getNbCarte(CarteWagon carte) {
        if (carte != null)
            return carteWagonsPossedees.get(carte);

        return -1;
    }

    public boolean ajouterDeltaCarteWagon(CarteWagon carte, int delta)
    {
        CarteWagon tmp = null;
        for (CarteWagon carteWagon : carteWagonsPossedees.keySet())
            if (carteWagon.equals(carte))
                tmp = carteWagon;

        if(tmp == null)
            return false;

        Integer nbCarte = carteWagonsPossedees.get(tmp);

        carteWagonsPossedees.remove(carte);
        carteWagonsPossedees.put(carte, nbCarte + delta);
        return true;
    }

    public boolean peutPrendre(Voie v, boolean estVoieDouble)
    {
        int nbLoco = v.getNbFerries();
        int nbLocoJoueur = getNbCarte(CarteWagon.typeCartes[CarteWagon.typeCartes.length - 1]);

        String couleur = "";
        if (estVoieDouble)
            couleur = v.getCouleur2();
        else
            couleur = v.getCouleur();

        if (couleur.equals("Gris"))
            return calculerNombresCartes() >= v.getNbTroncons();

        CarteWagon carteWagon = CarteWagon.getCarteParCouleur(couleur);

        if (nbLocoJoueur < nbLoco)
            return false;

        if (v.getNbTroncons() - getNbCarte(carteWagon) - nbLocoJoueur > 0)
            return false;

        return true;
    }

    private int calculerNombresCartes()
    {
        int ret = 0;
        for (CarteWagon carteWagon : CarteWagon.typeCartes)
            ret += getNbCarte(carteWagon);

        return ret;
    }

    public void retirerCarteDest(CarteDestination carte) {
        carteDestinationsPossedees.remove(carte);
    }

    public int getNbCarteWagonType(String type) {
        for (Map.Entry<CarteWagon, Integer> carte : carteWagonsPossedees.entrySet())
            if (carte.getKey().getCouleur().equals(type))
                return carte.getValue();

        return -1;
    }

    public CarteWagon getCarteWagonType(String type) {
        for (Map.Entry<CarteWagon, Integer> carte : carteWagonsPossedees.entrySet())
            if (carte.getKey().getCouleur().equals(type))
                return carte.getKey();

        return null;
    }

    public int getNbTypeCarte() {
        int ret = 0;
        for (CarteWagon carteWagon : CarteWagon.typeCartes)
            if (getNbCarte(carteWagon) != 0)
                ret ++;

        return ret;
    }
}
