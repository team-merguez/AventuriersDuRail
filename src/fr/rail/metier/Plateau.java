package fr.rail.metier;

import fr.rail.Controleur;
import fr.rail.metier.carte.CarteDestination;
import fr.rail.metier.carte.CarteWagon;
import fr.rail.util.FabriqueCarte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe stockant les pioches et les 5 cartes.
 */
public class Plateau
{
    List<CarteWagon>       carteDecouverte;
    List<CarteWagon>       piocheCarteWagon;
    List<CarteDestination> piocheCarteDestination;
    List<CarteWagon>       defausse;

    public Plateau(Controleur ctrl)
    {
        FabriqueCarte fabriqueCarte = new FabriqueCarte(ctrl.getEnsVille());

        piocheCarteWagon       = fabriqueCarte.genererPiocheCarteWagons();
        piocheCarteDestination = fabriqueCarte.genererPiocheCarteDestination();
        carteDecouverte        = new LinkedList<CarteWagon>();
        defausse               = new LinkedList<CarteWagon>();

        initialisationJeu(ctrl);

    }

    private void initialisationJeu(Controleur ctrl)
    {
        List<Joueur> ensJoueur = ctrl.getEnsJoueur();
        List<CarteDestination> ensCarteDestinationLongue = new ArrayList<>(piocheCarteDestination.subList(0, 6));
        piocheCarteDestination = new ArrayList<>(piocheCarteDestination.subList(6, piocheCarteDestination.size()));

        Collections.shuffle(piocheCarteWagon);
        Collections.shuffle(ensCarteDestinationLongue);
        Collections.shuffle(piocheCarteDestination);

        for (Joueur j : ensJoueur)
        {
            j.ajouterCarte(ensCarteDestinationLongue.remove(0));
            for (int i = 0; i < 4; i++)
                piocherCarteWagon(j);
            for (int i = 0; i < 3; i++)
                piocherCarteDestination(j);
        }

        for (int i = 0; i < 5; i++)
            carteDecouverte.add(piocheCarteWagon.remove(0));

    }

    public CarteDestination[] piocherCartesDestination(int nombre) {
        CarteDestination[] pioche = new CarteDestination[nombre];

        for (int i = 0; i < pioche.length; i++)
            pioche[i] = piocheCarteDestination.remove(0);

        return pioche;
    }

    public void piocherCarteDestination(Joueur j)
    {
        j.ajouterCarte(piocheCarteDestination.remove(0));
    }

    public CarteWagon piocherCarteWagon(Joueur j)
    {
        if (piocheCarteWagon.size() == 0) {
            piocheCarteWagon = new ArrayList<CarteWagon>(defausse);
            Collections.shuffle(piocheCarteWagon);
            defausse = new ArrayList<>();
        }


        CarteWagon tmp = piocheCarteWagon.remove(0);
        j.ajouterCarte(tmp);
        return tmp;
    }

    public CarteWagon piocherCarteWagonDecouverte(Joueur j, int i)
    {
        if (i < 0 || i > 4)
            return null;

        CarteWagon tmp = carteDecouverte.remove(i);
        j.ajouterCarte(tmp);
        carteDecouverte.add(i, piocheCarteWagon.remove(0));

        return tmp;
    }

    public void mettreCarteDestinationDessous(CarteDestination carteDestination)
    {
        piocheCarteDestination.add(carteDestination);
    }

    public void jeterCarteWagon(CarteWagon carteWagon)
    {
        defausse.add(carteWagon);
    }

    public List<CarteWagon> getCarteDecouverte() {
        return carteDecouverte;
    }
}
