package fr.rail.metier;

/**
 * Classe permettant de modéliser un Point avec ses coordonnées x et y.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 15/03/2017
 */
public class Point {
    private int posX;
    private int posY;

    public Point() {

    }

    public Point(int x, int y) {
        posX = x;
        posY = y;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }
}
