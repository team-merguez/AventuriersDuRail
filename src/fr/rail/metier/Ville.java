package fr.rail.metier;

import fr.rail.inter.IDessinable;

import java.util.ArrayList;

/**
 * Enumeration permettant de représenter une Ville.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 14/03/2017
 */
public class Ville implements IDessinable
{
    private String nom;
    private Point coordonnees;
    private ArrayList<Voie> ensVoie;

    /**
     * Constructeur d'une Ville.
     * @param nom  le nom de la Ville
     * @param posX l'abscisse de la Ville
     * @param posY l'ordonnée de la Ville
     */
    public Ville(String nom, int posX, int posY)
    {
        this.nom = nom;
        this.coordonnees = new Point(posX, posY);
        ensVoie = new ArrayList<>();
    }

    /**
     * @return le nom de la Ville
     */
    public String getNom() {
        return nom;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getX()
    {
        return coordonnees.getPosX();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getY()
    {
        return coordonnees.getPosY();
    }

    /**
     * @return les informations concernant la Ville courante
     */
    @Override
    public String toString() {
        return nom;
    }

    public void ajouterVoie(Voie voie)
    {
        if (!ensVoie.contains(voie))
            ensVoie.add(voie);
    }

    public ArrayList<Voie> getEnsVoie()
    {
        return ensVoie;
    }
}
