package fr.rail.metier;

/**
 * Classe permettant de représenter une Voie.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 14/03/2017
 */
public class Voie {
    private int numVoie;
    private Ville ville1;
    private Ville ville2;
    private int nbTroncons;
    private String couleur;
    private String couleur2;
    private int nbFerries;
    private boolean estTunnel;
    private Point[] ensTronconsVoie1;
    private Point[] ensTronconsVoie2;
    private Joueur joueurVoie1;
    private Joueur joueurVoie2;

    /**
     * Constructeur d'une Voie.
     *
     * @param numVoie    le numéro de la Voie
     * @param ville1     la première Ville
     * @param ville2     la deuxième Ville
     * @param nbTroncons le nombre de tronçons
     * @param couleur    la couleur de la Voie
     * @param nbFerries  le nombre de Ferries
     * @param estTunnel  indique si la Voie est un tunnel ou pas
     */
    public Voie(int numVoie, Ville ville1, Ville ville2, int nbTroncons,
                String couleur, int nbFerries, boolean estTunnel)
    {
        this.numVoie = numVoie;
        this.ville1 = ville1;
        this.ville2 = ville2;
        this.nbTroncons = nbTroncons;
        this.couleur = couleur;
        this.nbFerries = nbFerries;
        this.estTunnel = estTunnel;
        this.ensTronconsVoie1 = new Point[nbTroncons];
        this.ensTronconsVoie2 = new Point[nbTroncons];
    }

    public Ville getVille1()
    {
        return ville1;
    }

    public Ville getVille2()
    {
        return ville2;
    }

    public int getNumVoie() {
        return numVoie;
    }

    public int getNbTroncons() {
        return nbTroncons;
    }

    public String getCouleur() {
        return couleur;
    }

    public int getNbFerries() {
        return nbFerries;
    }

    public boolean isEstTunnel() {
        return estTunnel;
    }

    public Joueur getJoueurVoie1() {
        return joueurVoie1;
    }

    public void setJoueurVoie1(Joueur joueurVoie1) {
        this.joueurVoie1 = joueurVoie1;
    }

    public Joueur getJoueurVoie2() {
        return joueurVoie2;
    }

    public void setJoueurVoie2(Joueur joueurVoie2) {
        this.joueurVoie2 = joueurVoie2;
    }

    public Point[] getEnsTronconsVoie1() {
        return ensTronconsVoie1;
    }

    public Point[] getEnsTronconsVoie2() {
        return ensTronconsVoie2;
    }

    /**
     * Permet d'attribuer la seconde couleur d'une Voie (elle est donc double).
     *
     * @param couleur2 la seconde couleur
     */
    public void setCouleur2(String couleur2) {
        this.couleur2 = couleur2;
    }

    /**
     * @return les informations concernant la Voie courante
     */
    @Override
    public String toString() {
        return "Voie{" +
                "numVoie=" + numVoie +
                ", ville1=" + ville1 +
                ", ville2=" + ville2 +
                ", nbTroncons=" + nbTroncons +
                ", couleur='" + couleur + '\'' +
                ", nbFerries=" + nbFerries +
                ", estTunnel=" + estTunnel +
                '}';
    }

    /**
     * Méthode qui permet de décrire d'une manière élégante la Voie.
     *
     * @param  estVoieDouble true si la voie est double
     * @return une chaine comportant les informations sur la Voie
     */
    public String enChaine(boolean estVoieDouble)
    {
        return  "Voie numero " + numVoie + " reliant " + ville1.getNom() + " et " + ville2.getNom() + " avec la couleur " +
                (estVoieDouble?couleur2:couleur) + " et " + nbTroncons + " troncon" + (nbTroncons==1?" ":"s ") +
                (nbFerries!=0?"et " + nbFerries + " ferrie" + (nbFerries==1?"":"s"):"") + (estTunnel?"(Tunnel)":"");
    }

    public void setEnsTronconsVoie1(Point[] ensTronconsVoie1) {
        this.ensTronconsVoie1 = ensTronconsVoie1;
    }

    public void setEnsTronconsVoie2(Point[] ensTronconsVoie2) {
        this.ensTronconsVoie2 = ensTronconsVoie2;
    }

    public void setCoordTroncon(int voie, int troncon, Point p)
    {
        if (voie == 0)
            ensTronconsVoie1[troncon] = p;
        else if(voie == 1)
            ensTronconsVoie2[troncon] = p;
    }

    public String getCouleur2() {
        return couleur2;
    }

    public String toCsvLine()
    {
        String ret = "";

        ret+= numVoie + ";" + ville1.getNom() + ";" + ville2.getNom() + ";" + nbTroncons + ";" +
                couleur + ";" + nbFerries + ";" + (estTunnel? 'O':'N') + ";";

        for (int i = 0; i < 8; i++)
            ret+= i>=nbTroncons?";;": ensTronconsVoie1[i].getPosX() + ";" + ensTronconsVoie1[i].getPosY() + ";";

        if(couleur2 == null)
            return ret;

        ret+= "\n" + numVoie + ";" + ville1.getNom() + ";" + ville2.getNom() + ";" + nbTroncons + ";" +
                couleur2 + ";" + nbFerries + ";" + (estTunnel? 'O':'N') + ";";

        for (int i = 0; i < 8; i++)
            ret+= i>=nbTroncons?";;": ensTronconsVoie2[i].getPosX() + ";" + ensTronconsVoie2[i].getPosY() + ";";

        return ret;
    }

    public boolean prendreVoie(Joueur j, boolean voieDouble)
    {
        return false;
    }
}
