package fr.rail.metier.carte;

import fr.rail.metier.Joueur;

public abstract class Carte {

    protected boolean faceCachee;

    public void retournerCarte() {
        faceCachee = !faceCachee;
    }

    public abstract boolean prendre(Joueur j);
}
