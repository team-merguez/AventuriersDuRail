package fr.rail.metier.carte;

import fr.rail.metier.Joueur;
import fr.rail.metier.Ville;

/**
 * Created by alexi on 16/03/2017.
 */
public class CarteDestination extends Carte {

    /**
     * Nombre de points que vaut la destination.
     */
    private int nbPoints;

    /**
     * Les deux villes reliées par la carte destination.
     */
    private Ville[] villesReliees;

    /**
     * Constructeur d'une carte destination.
     *
     * @param nbPoints le nombre de points que vaut la carte destination
     * @param villesReliees les deux villes reliées par la carte destination
     */
    public CarteDestination(int nbPoints, Ville[] villesReliees) {
        if (villesReliees.length != 2)
            throw new IllegalArgumentException("Vous ne pouvez relier que deux villes.");

        this.nbPoints = nbPoints;
        this.villesReliees = villesReliees;
    }

    public boolean prendre(Joueur j) {
        return false;
    }

    public boolean estRouteLongue()
    {
        return nbPoints >= 20;
    }

    public Ville[] getVillesReliees() {
        return villesReliees;
    }

    public String toString() {
        return villesReliees[0] + " - " + villesReliees[1] + (estRouteLongue()?" (route longue) ":" ") + nbPoints + " points";
    }
}
