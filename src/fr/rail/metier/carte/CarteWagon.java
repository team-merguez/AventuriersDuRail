package fr.rail.metier.carte;

import fr.rail.metier.Joueur;

/**
 * Created by Quentin on 16/03/2017.
 */
public class CarteWagon extends Carte{

    public static final CarteWagon[] typeCartes = new CarteWagon[]{
            new CarteWagon("carte_blanche"),
            new CarteWagon("carte_bleue"),
            new CarteWagon("carte_jaune"),
            new CarteWagon("carte_noire"),
            new CarteWagon("carte_orange"),
            new CarteWagon("carte_rouge"),
            new CarteWagon("carte_verte"),
            new CarteWagon("carte_violette"),
            new CarteWagon("carte_loco")
    };

    private String image;

    private boolean estLocomotive;

    public CarteWagon(String image) {
        this.image = image;
        estLocomotive = image.equals("carte_loco");
    }

    public boolean prendre(Joueur j) {
        return j.ajouterCarte(this);
    }

    public String getCouleur()
    {
        String ret = image.substring(image.indexOf("_") + 1);
        switch (ret) {
            case "noire":
                ret = "noir";
                break;
            case "bleue":
                ret = "bleu";
                break;
            case "verte":
                ret = "vert";
                break;
            case "violette":
                ret = "violet";
                break;
            case "blanche":
                ret = "blanc";
                break;
        }
        return Character.toUpperCase(ret.charAt(0)) + ret.substring(1);
    }

    public String getImage() {
        return image;
    }

    public boolean equals(CarteWagon autre)
    {
        return image.equals(autre.image);
    }

    @Override
    public String toString()
    {
        return "CarteWagon : " + image;
    }

    public static CarteWagon getCarteParCouleur(String couleur)
    {
        for (CarteWagon carteWagon : typeCartes)
            if (carteWagon.image.substring(6).startsWith(couleur.toLowerCase()))
                return carteWagon;

        return null;
    }

    public boolean isEstLocomotive() {
        return estLocomotive;
    }
}
