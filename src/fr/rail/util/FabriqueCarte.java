package fr.rail.util;

import fr.rail.metier.Ville;
import fr.rail.metier.carte.Carte;
import fr.rail.metier.carte.CarteDestination;
import fr.rail.metier.carte.CarteWagon;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by alexi on 16/03/2017.
 */
public class FabriqueCarte {

    private List<Ville> ensVille;

    public FabriqueCarte(List<Ville> ensVille) {
        this.ensVille = ensVille;
    }

    public List<CarteDestination> extraireDepuis(String fichier) throws Exception {
        Scanner scFichier = new Scanner(new File(fichier));
        List<CarteDestination> ensCarteDestination = new ArrayList<>();

        while (scFichier.hasNextLine()) {
            ensCarteDestination.add(lireLigne(scFichier.nextLine()));
        }

        return ensCarteDestination;
    }

    private CarteDestination lireLigne(String ligne) throws Exception {
        String[] donnees = ligne.split(";");

        if (donnees.length != 3)
            throw new Exception("Ligne invalide : " + ligne + "\nIl y a trop d'arguments" );

        Ville[] villesReliees = new Ville[2];
        villesReliees[0] = LectureFichier.getVilleByName(donnees[0].trim(), (ArrayList<Ville>)ensVille);
        villesReliees[1] = LectureFichier.getVilleByName(donnees[1].trim(), (ArrayList<Ville>)ensVille);

        if (villesReliees[0] == null || villesReliees[1] == null)
            throw new Exception("Ligne invalide : " + ligne + "\nL'une des villes n'existe pas" );


        return new CarteDestination(Integer.parseInt(donnees[2]), villesReliees);
    }

    public List<CarteWagon> genererPiocheCarteWagons()
    {
        List<CarteWagon> ret = new LinkedList<CarteWagon>();


        for (int cpt = 0; cpt < CarteWagon.typeCartes.length - 1; cpt++)
            for (int i = 0; i < 12; i++)
                ret.add(CarteWagon.typeCartes[cpt]);

        for (int i = 0; i < 14; i++)
            ret.add(CarteWagon.typeCartes[CarteWagon.typeCartes.length - 1]);

        return ret;
    }

    public List<CarteDestination> genererPiocheCarteDestination()
    {
        try
        {
            return extraireDepuis("donnees/cartesDestinations.csv");
        } catch (Exception e) { e.printStackTrace(); }

        return null;
    }
}
