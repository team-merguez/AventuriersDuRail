package fr.rail.util;

import fr.rail.Controleur;
import fr.rail.metier.Point;
import fr.rail.metier.Ville;
import fr.rail.metier.Voie;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe utilitaire permettant de lire les fichiers .csv afin d'enregistrer les informations dans la mémoire.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 15/03/2017
 */
public class LectureFichier {

    /**
     * Méthode permettant de lire un fichier csv pour initialiser l'ensemble de Ville.
     *
     * @return la liste de Ville
     */
    public static ArrayList<Ville> lireVilles() {
        ArrayList<Ville> listeVille = new ArrayList<Ville>();

        Scanner sc;

        try {
            sc = new Scanner(new File("donnees/DonneesVilles.csv"));

            while (sc.hasNextLine()) {
                traiteLigneVille(sc.nextLine(), listeVille);
            }

            sc.close();
        }catch(Exception e) {
            System.out.println("Erreur dans la lecture du fichier de Ville");
        }

        return listeVille;
    }

    /**
     * Méthode permettant de lire un fichier csv pour initialiser l'ensemble de Voie.
     *
     * @return la liste de Voie
     */
    public static ArrayList<Voie> lireVoies(Controleur ctrl) {
        ArrayList<Voie>  listeVoie  = new ArrayList<Voie>();
        ArrayList<Ville> listeVille = ctrl.getEnsVille();

        Scanner sc;
        String ligne;

        try {
            sc = new Scanner(new File("donnees/DonneesVoies.csv"));

            while (sc.hasNextLine()) {
                if(!(ligne = sc.nextLine()).startsWith("#"))
                    traiteLigneVoie(ligne, listeVoie, listeVille);
            }

            sc.close();
        }catch(Exception e) {
            //System.out.println("Erreur dans la lecture du fichier de Voie");
            e.printStackTrace();
        }

        return listeVoie;
    }

    /**
     * Méthode qui permet de traiter une ligne du fichier csv.
     *
     * @param ligne      une ligne du fichier
     * @param listeVille l'ensemble des Ville
     */
    private static void traiteLigneVille(String ligne, ArrayList<Ville> listeVille) {
        String[] elems = ligne.split(";");
        listeVille.add(new Ville(elems[0], Integer.parseInt(elems[1]), Integer.parseInt(elems[2])));
    }

    /**
     * Méthode qui permet de traiter une ligne du fichier csv.
     *
     * @param ligne      une ligne du fichier
     * @param listeVille l'ensemble des Voie
     */
    private static void traiteLigneVoie(String ligne, ArrayList<Voie> listeVoie, ArrayList<Ville> listeVille) {
        String[] elems = ligne.split(";");
        int voie;
        Voie voieLu;


        if (Integer.parseInt(elems[0]) > listeVoie.size()) {
            voieLu = new Voie(Integer.parseInt(elems[0]), getVilleByName(elems[1], listeVille),
                    getVilleByName(elems[2], listeVille), Integer.parseInt(elems[3]), elems[4],
                    Integer.parseInt(elems[5]), elems[6].equals("O"));

            voie = 0;
            listeVoie.add(voieLu);

            voieLu.getVille1().ajouterVoie(voieLu);
            voieLu.getVille2().ajouterVoie(voieLu);
        }
        else
        {
            voieLu = listeVoie.get(Integer.parseInt(elems[0]) - 1);
            voieLu.setCouleur2(elems[4]);
            voie = 1;
        }

        for(int i = 7; i < elems.length && (i - 7)/2 < voieLu.getNbTroncons(); i+=2)
            voieLu.setCoordTroncon(voie, (i - 7)/2,
                    elems[i].equals("")?new Point():new Point(Integer.parseInt(elems[i]), Integer.parseInt(elems[i+1])));
    }

    /**
     * Méthode permettant de récupérer une instance de Ville grâce à son nom.
     *
     * @param  name     le nom de la Ville à rechercher
     * @param  ensVille l'ensemble de Ville
     * @return la Ville ayant le nom passé en paramètre
     */
    public static Ville getVilleByName(String name, ArrayList<Ville> ensVille)
    {
        for (int i = 0; i < ensVille.size() ; i++)
            if (ensVille.get(i).getNom().equals(name))
                return ensVille.get(i);

        return null;
    }
}
