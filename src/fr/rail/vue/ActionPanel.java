package fr.rail.vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by alexi on 17/03/2017.
 */
class ActionPanel extends JPanel implements MouseListener{
    private JLabel choixAction;
    private JPanel boutonsAction;
    private JButton action1, action2, action3;
    private Fenetre fenetre;
    public ActionPanel(Fenetre fen)
    {
        this.fenetre = fen;

        this.setLayout(new GridLayout(2,1));

        this.choixAction = new JLabel("Quelle action voulez-vous faire ?");
        this.choixAction.setHorizontalAlignment(this.getWidth()/2);
        this.add(this.choixAction);

        this.boutonsAction = new JPanel();
        this.boutonsAction.setLayout(new GridLayout(1,3));

        this.action1 = new JButton("Prendre des cartes wagons");
        this.action3 = new JButton("Prendre des cartes Destination supplémentaires");

        this.boutonsAction.add(action1);
        this.boutonsAction.add(action3);

        this.add(boutonsAction);

        this.action1.addMouseListener(this);
        this.action3.addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        if (e.getSource() == this.action1)
            fenetre.setPanel("piocheWagons");
        else if (e.getSource() == this.action3)
            fenetre.setPanel("prendreCarteDestination");
    }

    @Override
    public void mousePressed(MouseEvent e)
    {

    }

    @Override
    public void mouseReleased(MouseEvent e)
    {

    }

    @Override
    public void mouseEntered(MouseEvent e)
    {

    }

    @Override
    public void mouseExited(MouseEvent e)
    {

    }
}
