package fr.rail.vue;

import fr.rail.metier.Voie;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Classe permettant de gérer le Panel contenant la carte.
 *
 * @author Groupe 1 : Justin Allard, Alexis Godin, Erik Kubiak, Robin Lepetit, Quentin Marotte, Timothé Pardieu
 * @version du 14/03/2017
 */
public class CartePanel extends JPanel implements MouseListener {

    private Fenetre fenetreCarte;

    private Image carte;

    private BufferedImage carteCalque;

    /**
     * Constructeur d'un Panel contenant la carte.
     *
     * @param fenetreCarte la FenetreCarte qui contient le Panel.
     */
    CartePanel(Fenetre fenetreCarte) {
        this.fenetreCarte = fenetreCarte;

        try {
            carte = ImageIO.read(new File("donnees/carte_europe.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            carteCalque = ImageIO.read(new File("donnees/carte_troncons_europe.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setPreferredSize(new Dimension(carte.getWidth(null), carte.getHeight(null)));
        addMouseListener(this);

    }

    /**
     * Méthode qui permet de dessiner dans le Panel.
     *
     * @param g l'objet graphics utilisé
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        g.drawImage(carte, 0, 0, null);
        for (int i = 0; i < fenetreCarte.ctrl.getNbVoie(); i++) {
            Voie v = fenetreCarte.ctrl.getVoie(i);

            if (v.getJoueurVoie1() != null) {
                g.setColor(v.getJoueurVoie1().getCouleur().getCouleur());
                dessinerPointTroncon(g, v.getEnsTronconsVoie1());
            }

            if (v.getJoueurVoie2() != null) {
                g.setColor(v.getJoueurVoie2().getCouleur().getCouleur());
                dessinerPointTroncon(g, v.getEnsTronconsVoie2());
            }
        }
    }

    /**
     * Dessine les points sur un troncon.
     *
     * @param g                l'objet graphics utilisé
     * @param ensPointsTroncon les points du troncons
     */
    private void dessinerPointTroncon(Graphics g, fr.rail.metier.Point[] ensPointsTroncon) {

        for (fr.rail.metier.Point pointTroncon : ensPointsTroncon)
            g.fillOval(pointTroncon.getPosX() - 7, pointTroncon.getPosY() - 7, 14, 14);
    }

    /**
     * Méthode appellée lorsque l'utilisateur clique sur l'image.
     *
     * @param e l'événement de souris contenant les informations sur le clic
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        Color couleur = new Color(carteCalque.getRGB(e.getX(), e.getY()));

        if (couleur.getAlpha() == 0)
            return;

        if (couleur.getRed() != 0)
            fenetreCarte.ctrl.voieAction(couleur.getRed()-1, false);
        else if (couleur.getGreen() != 0)
            fenetreCarte.ctrl.voieAction(couleur.getGreen()-1, true);

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * Méthode qui permet de convertir une Image en une BufferedImage.
     *
     * @param image l'image à convertir
     * @return l'image convertit en BufferedImage
     */
    private BufferedImage toBufferedImage(Image image) {
        BufferedImage ret = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);

        ret.createGraphics().drawImage(image, 0, 0, null);

        return ret;
    }
}