package fr.rail.vue;

import fr.rail.Controleur;

import javax.swing.*;
import java.awt.*;

/**
 * Created by alexi on 17/03/2017.
 */
public class Fenetre extends JFrame {

    /**
     * Le controleur de l'application.
     */
    final Controleur ctrl;

    private CartePanel carte;

    private JoueurPanel joueur;

    private ActionPanel action;

    private PiochePanel pioche;

    /**
     * Constructeur de la FenetreCarte contenant l'image.
     *
     * @param ctrl le controleur
     */
    public Fenetre(Controleur ctrl) {
        this.ctrl = ctrl;

        setTitle("Les aventuriers du Rail");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize()));
        setLayout(new BorderLayout());

        // Création de la zone de la carte
        carte = new CartePanel(this);
        JScrollPane carteScrollPane = new JScrollPane(carte);
        carteScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        carteScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        // Création de la zone de la pioche
        action = new ActionPanel(this);
        pioche = new PiochePanel(this, "");

        // Création de la zone du joueur
        joueur = new JoueurPanel(ctrl);
        joueur.setJoueur(ctrl.getJoueurCourant());
        JScrollPane joueurScrollPane = new JScrollPane(joueur);
        joueurScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        // Ajout des compostants à la fenêtre
        add(carteScrollPane, BorderLayout.CENTER);
        add(joueurScrollPane, BorderLayout.EAST);

        this.add(action, BorderLayout.SOUTH);
        setVisible(true);
    }

    public void setPanel(String choix)
    {
            this.remove(action);
            pioche = new PiochePanel(this, choix);
            this.add(pioche, BorderLayout.SOUTH);
            revalidate();
            repaint();
    }

    public void changerJoueurCourant() {
        joueur.setJoueur(ctrl.getJoueurCourant());

        // Demande l'action au joueur
        remove(pioche);
        remove(action);
        action = new ActionPanel(this);
        add(action, BorderLayout.SOUTH);
        revalidate();
        repaint();
    }

    public void recherager() {
        carte.repaint();
        pioche.charger();
        joueur.setJoueur(ctrl.getJoueurCourant());
    }

    public Controleur getCtrl() {
        return ctrl;
    }
}
