package fr.rail.vue;

import fr.rail.Controleur;
import fr.rail.metier.Joueur;
import fr.rail.metier.carte.CarteDestination;

import javax.swing.*;
import java.awt.*;

public class FenetreCartesDest extends JFrame {

    public FenetreCartesDest(Controleur ctrl) {
        Joueur j = ctrl.getJoueurCourant();
        setLayout(new GridBagLayout());
        setLocation(50,50);
        setTitle("Visualisation des cartes destination");
        setSize(1100, 500);

        GridBagConstraints gc = new GridBagConstraints();

        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.insets = new Insets(0,2,0,2);
        gc.ipady = gc.anchor = GridBagConstraints.CENTER;
        gc.weightx = 10;
        gc.weighty = 2;

        gc.gridheight = 1;
        gc.gridwidth = 1;

        int cptX = 0;
        int cptY = 0;
        gc.gridy = cptY;
        for(CarteDestination carte : j.getCarteDestinationsPossedees()) {
            if(cptX % 6 == 0) {
                gc.gridy = cptY++;
                cptX = 0;
            }
            gc.gridx = cptX;
            add(new JLabel(carte.toString()), gc);
            gc.gridx = cptX+1;
            if(ctrl.cheminExistant(carte.getVillesReliees()[0], carte.getVillesReliees()[1], j))
                add(new JLabel(new ImageIcon((new ImageIcon("donnees/images/valide.png").getImage()
                        .getScaledInstance(70,70, Image.SCALE_DEFAULT)))), gc);
            else
                add(new JLabel(new ImageIcon((new ImageIcon("donnees/images/nonvalide.png").getImage()
                        .getScaledInstance(70,70, Image.SCALE_DEFAULT)))), gc);
            cptX+=2;
        }

        setVisible(true);
    }
}
