package fr.rail.vue;

import fr.rail.Controleur;
import fr.rail.metier.carte.CarteWagon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class FenetreChoix extends JFrame implements ActionListener {
    private final JButton confirmer;
    private final JTextField carteLoco;
    private final ArrayList<JTextField> cartesWagon;
    private final ArrayList<CarteWagon> cartesWagonsChoisies;
    private final String couleur;
    private Controleur ctrl;
    private boolean estVoieDouble;
    private int numVoie;

    public FenetreChoix(Controleur ctrl, int numVoie, boolean estVoieDouble) throws HeadlessException {
        this.ctrl = ctrl;
        this.estVoieDouble = estVoieDouble;
        this.numVoie = numVoie;
        cartesWagonsChoisies = new ArrayList<>();

        setTitle("Choix des cartes");
        setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize()));
        setLayout(new BorderLayout());

        cartesWagon = new ArrayList<JTextField>();

        if (estVoieDouble)
            couleur = ctrl.getVoie(numVoie).getCouleur2();
        else
            couleur = ctrl.getVoie(numVoie).getCouleur();

        CarteWagon cw = CarteWagon.getCarteParCouleur(couleur);
        JPanel pChoix;
        if (cw == null)
        {
            pChoix = new JPanel(new GridLayout(ctrl.getJoueurCourant().getNbTypeCarte(), 3));

            for (CarteWagon carteWagon1 : CarteWagon.typeCartes)
            {
                if (ctrl.getJoueurCourant().getNbCarte(carteWagon1) != 0 && !carteWagon1.isEstLocomotive())
                {
                    cartesWagonsChoisies.add(carteWagon1);
                    pChoix.add(new JLabel(new ImageIcon("donnees/images/" + carteWagon1.getImage() + ".jpg")));
                    JTextField tmp = new JTextField("0");
                    cartesWagon.add(tmp);
                    pChoix.add(tmp);
                    pChoix.add(new JLabel(" / " + ctrl.getJoueurCourant().getNbCarteWagonType(carteWagon1.getCouleur())));
                }
            }

            pChoix.add(new JLabel(new ImageIcon("donnees/images/carte_loco.jpg")));
            pChoix.add(carteLoco = new JTextField("0"));
            pChoix.add(new JLabel(" / " + ctrl.getJoueurCourant().getNbCarteWagonType("Loco")));
        }
        else {

            pChoix = new JPanel(new GridLayout(2, 3));

            pChoix.add(new JLabel(new ImageIcon("donnees/images/" + cw.getImage() + ".jpg")));
            JTextField tmp = new JTextField("0");
            cartesWagon.add(tmp);
            pChoix.add(tmp);
            pChoix.add(new JLabel(" / " + ctrl.getJoueurCourant().getNbCarteWagonType(couleur)));

            pChoix.add(new JLabel(new ImageIcon("donnees/images/carte_loco.jpg")));
            pChoix.add(carteLoco = new JTextField("0"));
            pChoix.add(new JLabel(" / " + ctrl.getJoueurCourant().getNbCarteWagonType("Loco")));
        }
        add(pChoix, BorderLayout.CENTER);

        confirmer = new JButton("Confirmer");
        confirmer.addActionListener(this);
        add(confirmer, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == confirmer) {
            ArrayList<CarteWagon> cartes = new ArrayList<>();

            System.out.println(cartesWagonsChoisies.size() + " " + cartesWagon.size());

            if (couleur.equals("Gris"))
            {
                for (int i = 0; i < cartesWagonsChoisies.size(); i++)
                    for (int j = 0; j < Integer.parseInt(!cartesWagon.get(i).getText().matches("\\d+")?"0":cartesWagon.get(i).getText()); j++)
                        cartes.add(cartesWagonsChoisies.get(i));
            }
            else
            {
                for (int i = 0; i < Integer.parseInt(!cartesWagon.get(0).getText().matches("\\d+")?"0":cartesWagon.get(0).getText()); i++)
                    cartes.add(ctrl.getJoueurCourant().getCarteWagonType(couleur));
            }

            for (int i = 0; i < Integer.parseInt(!carteLoco.getText().matches("\\d+")?"0":carteLoco.getText()); i++)
                cartes.add(ctrl.getJoueurCourant().getCarteWagonType("Loco"));

            if (!ctrl.carteValides(numVoie, estVoieDouble, cartes))
                return;

            ctrl.prendreVoie(numVoie, estVoieDouble, cartes);
            dispose();
        }
    }
}
