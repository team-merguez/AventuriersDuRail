package fr.rail.vue;

import fr.rail.Controleur;
import fr.rail.metier.carte.CarteDestination;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * Created by Quentin on 18/03/2017.
 */
public class FenetreChoixCarteDest extends JFrame implements ActionListener {

    private final Controleur ctrl;

    private JButton confirmer;

    private JCheckBox[] checkBoxes;

    private CarteDestination[] carteDestination;

    public FenetreChoixCarteDest(Controleur ctrl, CarteDestination[] carteDestinations) {
        this.ctrl = ctrl;
        this.carteDestination = carteDestinations;

        setTitle("Choisissz les cartes que vous voulez gardez");
        setLayout(new BorderLayout());

        add(new JLabel("Choisissez les cartes que vous voulez garder"), BorderLayout.NORTH);

        JPanel pChoixCartes = new JPanel(new GridLayout(carteDestinations.length, 2));

        checkBoxes = new JCheckBox[carteDestinations.length];

        for (int i = 0; i < carteDestinations.length; i++) {
            pChoixCartes.add(new JLabel(carteDestinations[i].toString()));
            pChoixCartes.add(checkBoxes[i] = new JCheckBox());
        }

        add(pChoixCartes, BorderLayout.CENTER);

        confirmer = new JButton("Confirmer");
        confirmer.addActionListener(this);
        add(confirmer, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == confirmer) {
            java.util.List<CarteDestination> select = new ArrayList<>();
            List<CarteDestination> nonSelect = new ArrayList<>();

            for (int i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].isSelected()) {
                    select.add(carteDestination[i]);
                } else {
                    nonSelect.add(carteDestination[i]);
                }
            }

            if (select.size() > 0) {
                ctrl.garderCartesDest(select, nonSelect);
                dispose();
            }
        }
    }
}
