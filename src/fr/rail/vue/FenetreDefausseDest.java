package fr.rail.vue;

import fr.rail.Controleur;
import fr.rail.metier.Joueur;
import fr.rail.metier.carte.Carte;

import javax.swing.*;
import java.awt.*;

public class FenetreDefausseDest extends JFrame {
    public FenetreDefausseDest(Controleur ctrl) {
        Joueur j = ctrl.getJoueurCourant();

        setLayout(new GridLayout(2, 4));
        setSize(700, 400);
        setTitle("Défausse de cartes destination");

        for(Carte carteDest : j.getCarteDestinationsPossedees()) {
            add(new JLabel(carteDest.toString()));
        }

        for(int i = 0 ; i < 4 ; i++) {
            //TODO
        }


        setVisible(true);
    }
}
