package fr.rail.vue;

import fr.rail.Controleur;
import fr.rail.metier.ECouleur;
import fr.rail.metier.Joueur;
import fr.rail.metier.carte.CarteWagon;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

class JoueurPanel extends JPanel {

    private Controleur ctrl;

    JoueurPanel(Controleur ctrl) {
        this.ctrl = ctrl;
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(200, getHeight()));
    }

    public void setJoueur(Joueur j) {
        removeAll();

        GridBagConstraints gc = new GridBagConstraints();

        gc.fill = GridBagConstraints.HORIZONTAL;
        gc.insets = new Insets(0,2,0,2);
        gc.ipady = gc.anchor = GridBagConstraints.CENTER;
        gc.weightx = 4;
        gc.weighty = 14;

        gc.gridx = 0;
        gc.gridy = 0;
        add(new JLabel("Joueur " + j.getPseudo()), gc);
        gc.gridx = 1;
        add(new Couleur(j.getCouleur()), gc);

        gc.gridx = 0;
        gc.gridy = 1;
        add(new JLabel("Score : "), gc);
        gc.gridx = 1;
        add(new JLabel(j.getScore() + ""), gc);

        gc.gridx = 0;
        gc.gridy = 2;
        add(new JLabel(new ImageIcon("donnees/images/" + j.getCouleur().getVoiture())), gc);
        gc.gridx = 1;
        add(new JLabel(j.getNbWagon() + " restants"), gc);

        new JLabel(new ImageIcon("donnees/images/"));

        int cpt = 3;
        for (Map.Entry<CarteWagon, Integer> carte : j.getCarteWagonsPossedees().entrySet()) {
            if(carte.getValue() != 0) {
                gc.gridx = 0;
                gc.gridy = cpt;
                add(new JLabel(new ImageIcon("donnees/images/" + carte.getKey().getImage() + ".jpg")), gc);
                gc.gridx = 1;
                add(new JLabel("x" + carte.getValue()), gc);
            }
            cpt++;
        }

        gc.gridx = 0;
        gc.gridy = cpt;
        add(new JLabel("Cartes destinations : "), gc);

        gc.gridx = 1;
        JButton bCarteDest = new JButton("Voir");
        bCarteDest.addActionListener(e -> new FenetreCartesDest(ctrl));
        add(bCarteDest, gc);

        revalidate();
        repaint();
    }

    private static class Couleur extends JPanel {

        private ECouleur couleur;

        Couleur(ECouleur couleur) {
            this.couleur = couleur;
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);

            g.setColor(couleur.getCouleur());
            g.fillRect(0, 0, 100, getHeight());
        }
    }
}
