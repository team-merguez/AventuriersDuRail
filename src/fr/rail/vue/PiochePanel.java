package fr.rail.vue;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by alexi on 17/03/2017.
 */
class PiochePanel extends JPanel {

    private Fenetre fenetre;
    private String action;

    PiochePanel(Fenetre fenetre, String action) {
        this.fenetre = fenetre;
        this.action = action;

        setPreferredSize(new Dimension(getWidth(), 100));

        charger();
    }

    void charger() {
        removeAll();

        JPanel pCartesVisibles = new JPanel();
        pCartesVisibles.setLayout(new GridLayout(1, 5));

        for (int i = 0; i < fenetre.ctrl.getPlateau().getCarteDecouverte().size(); i++)
            pCartesVisibles.add(new PiocheCarteVisible(fenetre.ctrl.getPlateau().getCarteDecouverte().get(i).getImage(), i));

        if (action.equals("piocheWagons"))
        {
            setLayout(new GridLayout(2, 2));

            add(new JLabel("Wagons"));
            add(new JLabel("Wagons visibles"));

            add(new PiocheWagon());
            add(pCartesVisibles);
        }
        else if (action.equals("prendrePossessionRoute"))
        {

        }
        else if (action.equals("prendreCarteDestination"))
        {
            setLayout(new GridLayout(2, 1));

            add(new JLabel("Destination"));

            add(new PiocheDestination());
        }

        revalidate();
        repaint();
    }

    private class PiocheWagon extends JPanel implements MouseListener {

        private BufferedImage piocheWagon;

        private PiocheWagon() {
            try {
                piocheWagon = ImageIO.read(new File("donnees/images/carte_dos.jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            setPreferredSize(new Dimension(piocheWagon.getWidth(null), piocheWagon.getHeight(null)));
            addMouseListener(this);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(piocheWagon, 0, 0, null);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            fenetre.ctrl.piocherCarteWagon();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class PiocheDestination extends JPanel implements MouseListener {

        private BufferedImage piocheWagon;

        private PiocheDestination() {
            try {
                piocheWagon = ImageIO.read(new File("donnees/images/carte_dos.jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            setPreferredSize(new Dimension(piocheWagon.getWidth(null), piocheWagon.getHeight(null)));
            addMouseListener(this);

        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(piocheWagon, 0, 0, null);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            fenetre.ctrl.piocherCarteDestination();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    private class PiocheCarteVisible extends JPanel implements MouseListener {

        private BufferedImage image;

        private int indice;

        private PiocheCarteVisible(String cheminImage, int indice) {
            this.indice = indice;

            try {
                image = ImageIO.read(new File("donnees/images/" + cheminImage + ".jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            setPreferredSize(new Dimension(image.getWidth(null), image.getHeight(null)));
            addMouseListener(this);
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, null);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            fenetre.ctrl.piocherCarteWagon(indice);
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}
